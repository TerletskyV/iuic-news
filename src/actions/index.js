import axios from 'axios';

export const GET_NEWS = 'GET_NEWS';
export const GET_NEWS_PENDING = 'GET_NEWS_PENDING';
export const GET_NEWS_FULFILLED = 'GET_NEWS_FULFILLED';
export const GET_NEWS_REJECTED = 'GET_NEWS_REJECTED';

export const GET_NEWS_ITEM = 'GET_NEWS_ITEM'
export const GET_NEWS_ITEM_PENDING = 'GET_NEWS_ITEM_PENDING';
export const GET_NEWS_ITEM_FULFILLED = 'GET_NEWS_ITEM_FULFILLED';
export const GET_NEWS_ITEM_REJECTED = 'GET_NEWS_ITEM_REJECTED';

export const GET_SLIDER_ITEMS = 'GET_SLIDER_ITEMS'
export const GET_SLIDER_ITEMS_PENDING = 'GET_SLIDER_ITEMS_PENDING';
export const GET_SLIDER_ITEMS_FULFILLED = 'GET_SLIDER_ITEMS_FULFILLED';
export const GET_SLIDER_ITEMS_REJECTED = 'GET_SLIDER_ITEMS_REJECTED';

export const GET_COUNTRIES_LIST = 'GET_COUNTRIES_LIST';
export const GET_COUNTRIES_LIST_PENDING = 'GET_COUNTRIES_LIST_PENDING';
export const GET_COUNTRIES_LIST_FULFILLED = 'GET_COUNTRIES_LIST_FULFILLED';
export const GET_COUNTRIES_LIST_REJECTED = 'GET_COUNTRIES_LIST_REJECTED';

export const GET_CITIES_LIST_OF_COUNTRY = 'GET_CITIES_LIST_OF_COUNTRY';
export const GET_CITIES_LIST_OF_COUNTRY_PENDING = 'GET_CITIES_LIST_OF_COUNTRY_PENDING';
export const GET_CITIES_LIST_OF_COUNTRY_FULFILLED = 'GET_CITIES_LIST_OF_COUNTRY_FULFILLED';
export const GET_CITIES_LIST_OF_COUNTRY_REJECTED = 'GET_CITIES_LIST_OF_COUNTRY_REJECTED';

export const GET_USER_INFO = 'GET_USER_INFO';
export const GET_USER_INFO_PENDING = 'GET_USER_INFO_PENDING';
export const GET_USER_INFO_FULFILLED = 'GET_USER_INFO_FULFILLED';
export const GET_USER_INFO_REJECTED = 'GET_USER_INFO_REJECTED';

export const GO_AWAY_NEWS = 'GO_AWAY_NEWS';

export const SUBSCRIBE_USER_INFO = 'SUBSCRIBE_USER_INFO';
export const SUBSCRIBE_USER_INFO_PENDING = 'SUBSCRIBE_USER_INFO_PENDING';
export const SUBSCRIBE_USER_INFO_FULFILLED = 'SUBSCRIBE_USER_INFO_FULFILLED';
export const SUBSCRIBE_USER_INFO_REJECTED = 'SUBSCRIBE_USER_INFO_REJECTED';

export const GET_SECTIONS = 'GET_SECTIONS';
export const GET_SECTIONS_PENDING = 'GET_SECTIONS_PENDING';
export const GET_SECTIONS_FULFILLED = 'GET_SECTIONS_FULFILLED';
export const GET_SECTIONS_REJECTED = 'GET_SECTIONS_REJECTED';

export const GET_SECTION_ID_NEWS = 'GET_SECTION_ID_NEWS';
export const GET_SECTION_ID_NEWS_PENDING = 'GET_SECTION_ID_NEWS_PENDING';
export const GET_SECTION_ID_NEWS_FULFILLED = 'GET_SECTION_ID_NEWS_FULFILLED';
export const GET_SECTION_ID_NEWS_REJECTED = 'GET_SECTION_ID_NEWS_REJECTED';

export const GET_CITY_NEWS = 'GET_CITY_NEWS';
export const GET_CITY_NEWS_PENDING = 'GET_CITY_NEWS_PENDING';
export const GET_CITY_NEWS_FULFILLED = 'GET_CITY_NEWS_FULFILLED';
export const GET_CITY_NEWS_REJECTED = 'GET_CITY_NEWS_REJECTED';

export const GET_USER_ADDRESS = 'GET_USER_ADDRESS';
export const GET_USER_ADDRESS_PENDING = 'GET_USER_ADDRESS_PENDING';
export const GET_USER_ADDRESS_REJECTED = 'GET_USER_ADDRESS_REJECTED';
export const GET_USER_ADDRESS_FULFILLED = 'GET_USER_ADDRESS_FULFILLED';

export const GET_COUNTRY_ID = 'GET_COUNTRY_ID';
export const GET_COUNTRY_ID_PENDING = 'GET_COUNTRY_ID_PENDING';
export const GET_COUNTRY_ID_REJECTED = 'GET_COUNTRY_ID_REJECTED';
export const GET_COUNTRY_ID_FULFILLED = 'GET_COUNTRY_ID_FULFILLED';

const URL = 'http://news.iuic.info/v1/web/news';

export function getNews(page = 1, per_page = 16, url = URL) {
  let params = {
    page: page,
    per_page: per_page
  }
  const request = axios.get(url, { params: params })
  return {
    type: GET_NEWS,
    payload: request
  }
}

export function getNewsItem(id) {
  const itemURL = URL + "/" + id
  const request = axios.get(itemURL)
  return {
    type: GET_NEWS_ITEM,
    payload: request
  }
}

export function goAwayNews() {
  return {
    type: GO_AWAY_NEWS
  }
}

export function getSliderItems() {
  const sliderItemURL = 'http://news.iuic.info/v1/web/addons/carousel';
  const request = axios.get(sliderItemURL)
  return {
    type: GET_SLIDER_ITEMS,
    payload: request
  }
}

export function getCountriesList() {
  const countriesURL = 'http://news.iuic.info/v1/countries';
  const request = axios.get(countriesURL)
  return {
    type: GET_COUNTRIES_LIST,
    payload: request
  }
}

export function getCitiesListOfCountry(url) {
  const request = axios.get(url)
  return {
    type: GET_CITIES_LIST_OF_COUNTRY,
    payload: request
  }
}

export function getUserInfo(id) {
  const userURL = 'http://news.iuic.info/v1/users';
  const userURLid = userURL + "/" + id;
  const request = axios.get(userURLid);
  return {
    type: GET_USER_INFO,
    payload: request
  }
}

export function subscribeUserInfo(responseFBook) {
  const url = "http://news.iuic.info/v1/web/users",
    data = {
      first_name: responseFBook.first_name,
      last_name: responseFBook.last_name,
      avatar_link: responseFBook.picture.data.url,
      uid: responseFBook.userID,
      provider: "fb"
    };

  let request = axios.post(url, data);

  return {
    type: SUBSCRIBE_USER_INFO,
    payload: request
  }
}

export function getSections() {
  const url = "http://news.iuic.info/v1/web/sections";
  const request = axios.get(url)
  return {
    type: GET_SECTIONS,
    payload: request
  }
}

export function getSectionIdNews(cityId, sectionId) {
  const reqUrl = URL + '/?city_id=' + cityId + '&section_id=' + sectionId;
  const request = axios.get(reqUrl)
  return {
    type: GET_SECTION_ID_NEWS,
    payload: request
  }
}

export function getCityNews(cityId) {
  const reqUrl = URL + '/?city_id=' + cityId;
  const request = axios.get(reqUrl)
  return {
    type: GET_CITY_NEWS,
    payload: request
  }
}

export function getUserAddress() {
  const reqUrl = "http://api.2ip.ua/geo.json?ip=";
  const request = axios.get(reqUrl)
    .then( (res) => {
      return axios.get("http://news.iuic.info/v1/countries/?q="+res.data.country_rus)
        .then( (res) => res.data.data[0] )
        .catch( (err) => console.log(err) )
    })
    .catch( (err) => { console.log(err) })
  return {
    type: GET_USER_ADDRESS,
    payload: request
  }
}