import React from 'react'
import { Link } from 'react-router-dom'
import Icon from 'react-fontawesome'
import FacebookLogin from 'react-facebook-login'
import Subscribed from './Subscribed'
import * as Actions from '../actions/index.js'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

class SocialSign extends React.Component {
  constructor(props) {
    super(props);
    this.state ={
      status: false,
      animationClass: "social-sign__fade-out"
    }
    this.responseFacebook = this.responseFacebook.bind(this);
  }

  responseFacebook(responseFBook) {
    this.props.actions.subscribeUserInfo(responseFBook);
    this.setState({
      status: true,
      animationClass: "social-sign__fade-out"
    });
  }

  componentDidMount() {
    const isLogged = window.localStorage.getItem("logged");
    if (isLogged === "true") {
      this.setState({
        status: true,
        animationClass: "social-sign__fade-out"
      })
    } else {
      this.setState({
        status: false,
        animationClass: "social-sign__fade-in"
      })
    }
  }

  render () {
    return (
      <div className="status-bar">
        {this.state.status ? <Subscribed /> :
          <div  className={"top-bar__social-sign social-sign " + this.state.animationClass}>
            <p className="social-sign__text-caption">Войти с помощью:</p>
            <FacebookLogin
              appId="1584420094902355"
              autoLoad={true}
              fields="first_name, last_name, email, picture"
              cssClass="social-sign__btn social-sign__btn--facebook"
              icon="fa-facebook"
              textButton=""
              tag="button"
              callback={this.responseFacebook}>
            </FacebookLogin>
            <Link to="" className="social-sign__btn social-sign__btn--vk">
              <Icon
                name="vk"
                tag="i"/>
            </Link>
          </div>
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    userState: state.userState
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SocialSign);