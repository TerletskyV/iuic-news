import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Masonry from 'react-masonry-component'
import NewsCard from '../components/NewsCard'
import { Container, Col } from 'reactstrap'

let newsItems = new Map(),
    serverUrl = "http://news.iuic.info";

class News extends Component {
  constructor (props) {
    super(props)

    this.onScrollNews = this.onScrollNews.bind(this)
  }

  onScrollNews() {
    if (window.pageYOffset + window.innerHeight > (document.body.scrollHeight - 116)) {
      this.props.onFetchNews(this.props.page)
      window.removeEventListener('scroll', this.onScrollNews)
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onScrollNews)
  }

  componentDidUpdate() {
    window.addEventListener('scroll', this.onScrollNews)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScrollNews)
    newsItems.clear()
    this.props.onGoAwayNews()
  }

  componentWillReceiveProps(nextProps) {
    console.log(!!nextProps.news)
    console.log(!nextProps.news.list.length)
    if (!!nextProps.news && !nextProps.news.list.length) {
      newsItems.clear();
      newsItems.set(1, <h2 className="message__not-found">Еще нет публикаций</h2> )
    } else {
      newsItems.clear()
    }
    if (nextProps.location && nextProps.location.pathname === "/country") {
      console.log(nextProps.location.pathname)
    }
  }

  render() {
    this.props.news.list.map((article) => {
      newsItems.set(article.id,
        <NewsCard
          key={article.id}
          mainUrl={"/news/" + article.id}
          mainImg={!!article.attributes.title_screenshot.mobile.url ? serverUrl + article.attributes.title_screenshot.mobile.url : ''}
          title={article.attributes.title_ru}
          cityUrl={"/city/" + article.attributes.city_id}
          cityName={article.attributes.city_name_ru}
          date={article.attributes.created_at}
          user_id={article.attributes.user.id}
          user_avatar={article.attributes.user.avatar.url != null ? serverUrl + article.attributes.user.avatar.url : article.attributes.user.avatar_link}
          user_name={article.attributes.user.first_name + ' ' + article.attributes.user.last_name}
          like={article.attributes.news_like.voices}
          dislike={article.attributes.news_dislike.voices}
          views={article.attributes.views}>
        </NewsCard>
      );
      return false;
    });
    return (
      <Container fluid className="custom-offset">
        <Masonry
          className="news"
          onScroll={this.onScrollNews.bind(this)}>
          {newsItems}
        </Masonry>
      </Container>
    );
  }
}

News.propTypes = {
  onFetchNews: PropTypes.func.isRequired
}

export default News