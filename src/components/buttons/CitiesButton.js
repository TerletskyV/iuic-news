import React, { Component } from 'react';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

class CitiesButton extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.searchCity = this.searchCity.bind(this);
    this.selectCity = this.selectCity.bind(this);
    this.state = {
      dropdownOpen: false,
      searchButton: 'города страны',
      searchCityValue: '',
      cities: []
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
    if(typeof this.state.searchButton === 'string') {
      this.setState({
          searchButton:
              <input
                autoFocus
                className="country-button__search-field"
                type="text"
                onChange={this.searchCity}>
              </input>
        });
    } else {
      this.setState({
        searchButton: 'города страны'
      })
    }
  }

  selectCity(event) {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
      searchButton: event.target.textContent
    });
  }

  searchCity(event) {
    this.setState({
      searchCityValue: event.target.value
    });
  }

  componentWillReceiveProps(nextProps) {
    let citiesData = nextProps.cities.cities,
      cities = [];
    let countryCities = nextProps.countryCities;

    if (citiesData.length) {
      citiesData.forEach( (item) => cities.push(item.cityNameRu) )
      this.setState({
        cities: cities
      });
    } else if (nextProps.countryCities && nextProps.countryCities.length) {
      countryCities.forEach( (item) => cities.push(item.name_ru) )
      this.setState({
        cities: cities
      });
    }
  }

  render() {
    let cities = this.state.cities;
    return(
      <ButtonDropdown
        className="country-button__container"
        isOpen={this.state.dropdownOpen}
        toggle={this.state.cities.length ? this.toggle : ()=>{}}>
        <DropdownToggle className="country-button__btn">
          {this.state.searchButton}
        </DropdownToggle>
        <DropdownMenu onClick={this.state.cities.lentgh ? this.selectCity : ()=>{} }>
          {cities.map((item, key) => {
            if ( item.toLowerCase().indexOf( (this.state.searchCityValue).toLowerCase() ) !== -1 ) {
              return <DropdownItem key={key}>{item}</DropdownItem>
            }
          })}
        </DropdownMenu>
      </ButtonDropdown>
    );
  }
}

export default CitiesButton;