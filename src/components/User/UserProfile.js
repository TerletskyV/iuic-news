import React, { Component } from 'react';
import Profile from '../../containers/Profile';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../actions';

class UserProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userAvatar: '',
      userAge: '21.12.1967г.',
      userAddress: 'Ukraine, Kharkiv'
    };
  }

  componentDidMount() {
    let userId = this.props.location.pathname.match(/\d+/g);
    this.props.actions.getUserInfo(userId);
  }

  componentWillReceiveProps(nextProps) {
    let avatarSrc;
    console.log(nextProps)
    if(!nextProps.user.pending) {
      if(nextProps.user.info.provider === 'fb') {
        avatarSrc = 'https://graph.facebook.com/' + nextProps.user.info.uid + '/picture?type=large'
      } else {
        avatarSrc = 'http://news.iuic.info' + nextProps.user.info.avatar.url;
      }
      this.setState({
        userAvatar: avatarSrc
      });
    }
  }

  render() {
    return(
      <Profile
        avatar={this.state.userAvatar}
        name={this.props.user.info.first_name + ' ' + this.props.user.info.last_name}
        age={this.state.userAge}
        address={this.state.userAddress}>
      </Profile>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);