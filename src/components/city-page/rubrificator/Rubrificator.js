import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as Actions from '../../../actions'
class Rubrificator extends Component {
  constructor(props) {
    super(props);
    this.getSectionIdNews = this.getSectionIdNews.bind(this);
    this.state = {
      btnActive: null
    }
  }

  getSectionIdNews(event) {
    const city_id = +this.props.city_id;
    let section_id = event.currentTarget.dataset.value;
    this.props.actions.getSectionIdNews(city_id, section_id);
    this.setState({
      btnActive: +section_id
    });
  }

  render() {
    console.log(this)
    const city_id = this.props.city_id;
    return(
      <div className="rubrificator">
        <ul className="cl-effect-4">
          <li>
            <Link to={"/city/" + city_id} className={this.state.btnActive === 2 ? "rubrificator__btn--active btn" : "rubrificator__btn btn"} data-value="2" onClick={this.getSectionIdNews}>
              <span className="btn__img-container">
                <img className="btn__img" src="/rubrificator/town-news-micro.png" alt="" />
              </span>
              <span className="btn__text">Новости города</span>
            </Link>
          </li>
          <li>
            <Link to={"/city/" + city_id} className={this.state.btnActive === 3 ? "rubrificator__btn--active btn" : "rubrificator__btn btn"} data-value="3" onClick={this.getSectionIdNews}>
              <span className="btn__img-container">
                <img className="btn__img" src="/rubrificator/rumor.png" alt="" />
              </span>
              <span className="btn__text">Молва людей</span>
            </Link>
          </li>
          <li>
            <Link to={"/city/" + city_id} className={this.state.btnActive === 1 ? "rubrificator__btn--active btn" : "rubrificator__btn btn"} data-value="1" onClick={this.getSectionIdNews}>
              <span className="btn__img-container">
                <img className="btn__img" src="/rubrificator/opoveshenia.png" alt="" />
              </span>
              <span className="btn__text">Оповещения</span>
            </Link>
          </li>
          <li>
            <Link to={"/city/" + city_id} className={this.state.btnActive === 4 ? "rubrificator__btn--active btn" : "rubrificator__btn btn"} data-value="4" onClick={this.getSectionIdNews}>
              <span className="btn__img-container">
                <img className="btn__img" src="/rubrificator/sherlock.png" alt="" />
              </span>
              <span className="btn__text">Розыск и находки</span>
            </Link>
          </li>
          <li>
            <Link to={"/city/" + city_id} className={this.state.btnActive === 5 ? "rubrificator__btn--active btn" : "rubrificator__btn btn"} data-value="5" onClick={this.getSectionIdNews}>
              <span className="btn__img-container">
                <img className="btn__img" src="/rubrificator/afisha.png" alt="" />
              </span>
              <span className="btn__text">Афиша города</span>
            </Link>
          </li>
          <li>
            <Link to={"/city/" + city_id} className={this.state.btnActive === 6 ? "rubrificator__btn--active btn" : "rubrificator__btn btn"} data-value="6" onClick={this.getSectionIdNews}>
              <span className="btn__img-container">
                <img className="btn__img" src="/rubrificator/soveti.png" alt="" />
              </span>
              <span className="btn__text">Советы людей</span>
            </Link>
          </li>
        </ul>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    sectionIdNews: state.sectionIdNews
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Rubrificator)
