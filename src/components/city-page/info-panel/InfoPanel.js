import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import DateTimeWidget from './widgets/DateTimeWidget';
import CurrencyWidget from './widgets/CurrencyWidget';
import WeatherWidget from './widgets/WeatherWidget';
import About from './About';

class InfoPanel extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="town-info-panel">
        <Row>
          <Col lg="4">
            <div className="town-info-panel__widgets">
              <DateTimeWidget className="town-info-panel__widget" />
              <CurrencyWidget className="town-info-panel__widget"
                currency={this.props.city_info.currency}>
              </CurrencyWidget>
              <WeatherWidget className="town-info-panel__widget" />
            </div>
          </Col>
          <Col lg="8">
            <About
              city_info={this.props.city_info}>
            </About>
          </Col>
        </Row>
      </div>
    );
  }
}

export default InfoPanel;