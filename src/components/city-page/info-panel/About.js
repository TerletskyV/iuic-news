import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class About extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="town-info-panel__about about">
        <div className="about__container">
          <Link to="" className="town-info-panel__admin-btn">Админ города</Link>
        </div>
        <div className="about__container countru">
          <div className="country__container">
            <span>
              <img className="about__img" src="" alt="" />
            </span>
            {this.props.city_info.country}
          </div>
        </div>
        <div className="about__container city">
          <div className="city__container">
            <span>
              <img className="about__img" src={this.props.city_info.coatImg} alt="" />
            </span>
            {this.props.city_info.city}
          </div>
        </div>
      </div>
    );
  }
}

export default About;