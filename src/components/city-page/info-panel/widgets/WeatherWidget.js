import React, { Component } from 'react';

class WetherWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      meteo: "/meteo/d.sun.png",
      fahrenheit: "?",
      celsius: "?"
    }
  }

  render() {
    return(
      <div className={this.props.className ? this.props.className + " wether" : "wether"}>
        <div className="wether__icon">
          <img src={this.state.meteo} alt="" />
        </div>
        <div className="wether__temp temp">
          <span className="temp__container temp__container--fahrenheit">
            F&deg; : {this.state.fahrenheit}
          </span>
          <span className="temp__container temp__container--celsius">
            C&deg; : {this.state.celsius}
          </span>
        </div>
      </div>
    );
  }
}

export default WetherWidget;