import React, { Component } from 'react';
import Time from 'react-time';
import { ru } from 'moment/locale/ru';

class DateTimeWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      now: new Date(),
      timerId: false
    };
  }

  componentDidMount() {
    let that = this;
    let date;
    let timerId = setInterval( () => {
      date = new Date();
      that.setState({
        now: date
      });
    }, 60000 );
    this.setState({
      timerId: timerId
    });
  }

  componentWillUnmount() {
    clearInterval(this.state.timerId);
  }

  render() {
    let now = this.state.now;
    return(
      <div className={this.props.className ? this.props.className + " date-time" : "date-time"}>
        <div className="date-time__container date-time__container--time">
          <Time value={now} format="LT" locale={ru} />
        </div>
        <div className="date-time__container date-time__container--week-day">
          <Time value={now} format="dddd" locale={ru} />
        </div>
        <div className="date-time__container date-time__container--date">
          <Time value={now} format="L" locale={ru} />
        </div>
      </div>
    );
  }
}

export default DateTimeWidget;