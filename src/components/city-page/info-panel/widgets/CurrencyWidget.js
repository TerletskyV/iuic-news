import React, { Component } from 'react';

class CurrencyWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currency: '',
      eur: '?.??',
      usd: '?.??',
      rub: '?.??'
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      currency: nextProps.currency
    })
  }

  render() {
    return (
      <div className={this.props.className ? this.props.className + " widget-currency" : "widget-currency"}>
        <div className="widget-currency__container widget-currency__container--name">
          {this.state.currency}
        </div>
        <div className="widget-currency__container widget-currency__container--USD">
          {"USD " + this.state.usd}
        </div>
        <div className="widget-currency__container widget-currency__container--RUB">
          {"RUB " + this.state.rub}
        </div>
      </div>
    );
  }
}

export default CurrencyWidget;