import React, { Component } from 'react'
import Masonry from 'react-masonry-component'
import NewsCard from '../NewsCard'
import axios from 'axios'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as Actions from '../../actions'

class CityNews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city_news: new Map()
    }
  }

  componentWillReceiveProps(nextProps) {
    let cityNewsArr = new Map();
    const cityNews = nextProps.sectionIdNews.content,
      serverUrl = "http://news.iuic.info";

    if (cityNews.length) {
      cityNews.map( (article) => {
        cityNewsArr.set(article.id,
          <NewsCard
            key={article.id}
            mainUrl={"/news/" + article.id}
            mainImg={!!article.attributes.title_screenshot.mobile.url ? serverUrl + article.attributes.title_screenshot.mobile.url : ''}
            title={article.attributes.title_ru}
            cityUrl={"/city/" + article.attributes.city_id}
            cityName={article.attributes.city_name_ru}
            date={article.attributes.created_at}
            user_id={article.attributes.user.id}
            user_avatar={article.attributes.user.avatar.url != null ? serverUrl + article.attributes.user.avatar.url : article.attributes.user.avatar_link}
            user_name={article.attributes.user.first_name + ' ' + article.attributes.user.last_name}
            like={article.attributes.news_like.voices}
            dislike={article.attributes.news_dislike.voices}
            views={article.attributes.views}>
          </NewsCard>
        )
      })
      this.setState({
        city_news: cityNewsArr
      });
    } else {
      this.setState({
        city_news: <h2 className="message__not-found">Ещё нет публикаций</h2>
      });
    }
  }

  render() {
    let city_news = this.state.city_news;
    return(
      <Masonry>{city_news}</Masonry>
    );
  }
}


function mapStateToProps(state) {
  return {
    sectionIdNews: state.sectionIdNews
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CityNews)