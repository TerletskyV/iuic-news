import React, { Component } from 'react'
import TimeAgo from 'react-timeago'
import ruStrings from 'react-timeago/lib/language-strings/ru'
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter'
import { Link } from 'react-router-dom'
import { Col } from 'reactstrap'
import ReactAudioPlayer from 'react-audio-player'
import axios from 'axios'

class Comment extends Component {
  constructor(props) {
    super(props);
    this.play = this.play.bind(this);
    this.state = {
      avatar_link: "",
      user_name: "",
      play: "play-button.png",
      playState: true
    }
  }

  play() {
    let audio = this.rap.audioEl;
    if (this.state.playState) {
      this.setState({
        playState: !this.state.playState,
        play: "stop-button.png"
      });
      audio.play();
    } else {
      this.setState({
        playState: !this.state.playState,
        play: "play-button.png"
      });
      audio.pause();
    }
  }

  componentWillMount() {
    const _this = this;
    axios.get("http://news.iuic.info/v1/web/users/" + this.props.user)
      .then(res => _this.setState({
        avatar_link: res.data.data.attributes.avatar_link,
        user_name: res.data.data.attributes.first_name + ' ' + res.data.data.attributes.last_name
      }))
      .catch(err => console.log(err))
  }

  render() {
    const formatter = buildFormatter(ruStrings);
    return(
      <Col className="comment">
        <Link className="user" to={"/user/" + this.props.user}>
          <img className="user__avatar" src={this.state.avatar_link} alt="avatar" />
        </Link>
        <div className="user">
          <Link className="user__name" to={"/user/" + this.props.user}>{this.state.user_name}</Link>
        </div>
        <div className="">
          <TimeAgo className="author__date date" date={this.props.date} formatter={formatter} />
        </div>
        <div className="comment__audio-player" onClick={this.play}>
          <img className="comment__play-button" src={"/" + this.state.play} alt="audio-player-button" />
          <ReactAudioPlayer
            onEnded={this.play}
            src={this.props.audio}
            ref={(element) => { this.rap = element; }}>
          </ReactAudioPlayer>
        </div>
      </Col>
    );
  }
}

export default Comment