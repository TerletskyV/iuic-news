import React from 'react';
import Slider from '../Slider';
import CountryButton from '../buttons/CountryButton';
import CitiesButton from '../buttons/CitiesButton';

const MainCountrySlider = () => {
  <div className="iuic-news__main-slider">
    <Slider location="country news" />
  </div>
}

export default MainCountrySlider;