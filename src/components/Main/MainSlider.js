import React from 'react';
import Slider from '../Slider';
import CountryButton from '../buttons/CountryButton';
import CitiesButton from '../buttons/CitiesButton';

const MainSlider = () => (
  <div className="iuic-news__main-slider">
    <Slider location="world news" />
  </div>
);

export default MainSlider;