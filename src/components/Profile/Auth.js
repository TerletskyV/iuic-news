import React from 'react';
import Icon from 'react-fontawesome';
import { Button, Input, InputGroup } from 'reactstrap';

export default class Auth extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <form className="profile__auth auth">
        <div className="auth__methods">
          <span>Создать кабинет с помощью:</span>
          <Button
            color="link"
            className="auth__method facebook"
            type="button">
            <Icon name="facebook"/>
          </Button>
          <Button
            color="link"
            className="auth__method vk"
            type="button">
            <Icon name="vk"/>
          </Button>
        </div>
      </form>
    )
  }
}