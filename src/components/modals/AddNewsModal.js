import React, { Component } from 'react'
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup,
  Label, Input, Row, Col, FormText } from 'reactstrap'
import Dropzone from 'react-dropzone'
import * as Actions from '../../actions/index.js'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import axios from 'axios'

class AddNewsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      size: 'lg',
      modal: false,
      sections: [],
      countries: [],
      cities: [],
      cityState: true,
      files: [],
      user_id: this.props.userState.userId,
      title_ru: '',
      description_ru: ''
    };
    this.toggle = this.toggle.bind(this);
    this.selectCountry = this.selectCountry.bind(this);
    this.onDrop = this.onDrop.bind(this);
    this.send = this.send.bind(this);
  }

  send(event) {
    event.preventDefault();
    const form = document.forms.news,
      _this = this,
      fd = new FormData(form),
      url = "http://news.iuic.info/v1/web/news";

    if (this.state.filesWereDropped) {
        this.state.files.forEach(file => {
            fd.append('news[screenshot_data][]', file, file.name);
        });
    }
    fd.append("news[user_id]", _this.state.user_id)
    axios.post(url, fd)
      .then(function(res) {
        console.log(res);
        _this.props.actions.goAwayNews();
        _this.toggle();
      })
      .catch(function(err) {
        console.log(err)
      })
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  onDrop(acceptedFiles, rejectedFiles, e) {
    this.setState({
        files: acceptedFiles,
        filesWereDropped: !e.target.files || e.target.files.length === 0
    });
    console.log(this.state)
  }

  selectCountry(event) {
    let country_id = event.target.value,
      url = "http://news.iuic.info/v1/countries/" + country_id;
    this.props.actions.getCitiesListOfCountry(url);
    this.setState({
      cityState: false
    });
  }

  setTitleValue(event) {
    this.setState({
      title_ru: event.target.value
    })
  }

  setDescriptionValue(event) {
    this.setState({
      description_ru: event.target.value
    })
  }

  componentDidMount() {
    this.props.actions.getSections();
    let btn = document.querySelector('#create');
    btn.addEventListener('click', this.toggle);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      countries: nextProps.countries.countries > this.state.countries ? nextProps.countries.countries : this.state.countries,
      cities: nextProps.cities.cities > this.state.cities ? nextProps.cities.cities : this.state.cities,
      sections: nextProps.sections.sections > this.state.sections ? nextProps.sections.sections : this.state.sections,
      user_id: nextProps.userState.userID
    })
  }

  render() {

    const dropzoneMessage = () =>
      <div className="create-news__dropzone-message">
        <div className="create-news__dropzone-image">
          <img className="create-news__dropzone-icon" src="download.svg" alt="" />
        </div>
        <p className="create-news__dropzone-caption">Перетащите изображения сюда, чтобы загрузить</p>
      </div>;

    const preview = () =>
      <div className="create-news__dropzone-message">
        {this.state.files.map((file, index) => <img className="create-news__dropzone-preview" key={index} src={file.preview} /> )}
      </div>;

    return (
      <Modal
        isOpen={this.state.modal}
        toggle={this.toggle}
        size={this.state.size}
        className="create-news">
        <ModalHeader toggle={this.toggle} className="graygreen">Создать публикацию</ModalHeader>
        <ModalBody>
          <Form name="news">
            <FormGroup>
              <Label className="create-news__label" for="title_ru">
                <span className="create-news__for-requred">* </span> Название
              </Label>
              <Input
                type="text"
                name="news[title_ru]"
                id="title_ru"
                value={this.state.title_ru}
                onChange={this.setTitleValue.bind(this)}>
              </Input>
            </FormGroup>
            <FormGroup>
              <Label className="create-news__label" for="description_ru">
                <span className="create-news__for-requred">* </span> Описание
              </Label>
              <Input
                type="text" name="news[description_ru]"
                id="description_ru"
                value={this.state.description_ru}
                onChange={this.setDescriptionValue.bind(this)}>
              </Input>
            </FormGroup>
            <FormGroup>
              <Label className="create-news__label" for="description_ru">
                <span className="create-news__for-requred">* </span> Раздел
              </Label>
              <Input
                className="col-md-3"
                type="select"
                name="news[section_id]"
                id="section">
                {this.state.sections.map( (item, index) => <option key={index} value={item.section_id}>{item.section_name_ru}</option> )}
              </Input>
            </FormGroup>
            <FormGroup>
              <h3 className="create-news__header-3">Добавьте изображение</h3>
              <Dropzone
                onDrop={this.onDrop}
                disablePreview={false}
                name="news[screenshot_data][]"
                className="create-news__dropzone-previews"
                activeClassName="create-news__dropzone--active">
                { this.state.files.length > 0 ? preview() : dropzoneMessage() }
              </Dropzone>
            </FormGroup>
            <FormGroup>
              <h3 className="create-news__header-3">Добавьте видеоматериал</h3>
              <Label className="create-news__label" for="youtube_link">
                 Cсылка на видео YouTube
              </Label>
              <Input type="text" name="news[youtube_link]" id="youtube_link" />
            </FormGroup>
            <FormGroup>
              <h3 className="create-news__header-3">Укажите местоположение</h3>
              <Row>
                <Col md="3">
                  <Label for="">
                    <span className="create-news__for-requred">* </span> Страна
                  </Label>
                  <Input type="select" name="news[country_id]" onChange={this.selectCountry}>
                    {this.state.countries.map( (item, index) => <option key={index} value={item.id}>{item.name_ru}</option> )}
                  </Input>
                </Col>
                <Col md="3">
                  <Label for="">
                    <span className="create-news__for-requred">* </span> Город
                  </Label>
                  <Input type="select" name="news[city_id]" disabled={this.state.cityState}>
                    {this.state.cities.map( (item, index) => <option key={index} value={item.cityId}>{item.cityNameRu}</option> )}
                  </Input>
                </Col>
              </Row>
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.send}>Создать</Button>
        </ModalFooter>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    news: state.news,
    sections: state.sections,
    cities: state.cities,
    userState: state.userState
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddNewsModal);