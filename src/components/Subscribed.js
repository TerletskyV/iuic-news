import React, { Component } from 'react'
import * as Actions from '../actions/index.js'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Link } from 'react-router-dom'

class Subscribed extends Component {
  constructor(props) {
    super(props);
    this.state ={
      animationClass: "social-sign__fade-out"
    }
  }

  logOut() {
    let localStor = window.localStorage;

    localStor.setItem("logged", false);
    localStor.removeItem('userId');
    localStor.removeItem('userFirstName');
    localStor.removeItem('userLastName');
    localStor.removeItem('userAvatar');

    this.setState({
      animationClass: "social-sign__fade-out"
    })
  }

  componentDidMount() {
    this.setState({
      animationClass: "social-sign__fade-in"
    })
  }

  render () {
    return (
      <div  className={"top-bar__subscribed " + this.state.animationClass}>
        <Link to="/profile" className="into-profile">
          <span className="user-avatar">
            <img alt="Аватар пользователя" className="img-thumbnail top-bar__user-avatar" src={this.props.userState.userAvatar} />
          </span>
        </Link>
        <a className="btn btn-link log-out" href="/" onClick={this.logOut.bind(this)}>Выход</a>
        <div className="notifications">
          <i className="fa fa-bell-o"></i>
          <span>  0</span>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    userState: state.userState
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Subscribed);