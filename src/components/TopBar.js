import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Icon from 'react-fontawesome'
import { Button } from 'reactstrap'
import SocialSign from './SocialSign'

class TopBar extends Component {
  render() {
    return (
      <div className="top-bar">
        <Button
          color="link"
          size="sm"
          className="top-bar__about">
          <Icon
            name="bars"
            tag="i"/>
        </Button>
        <Link to="/"
          className="top-bar__link">
          <img
            className="top-bar__logo"
            src={process.env.PUBLIC_URL + "/logo-news.png"}
            alt="IUIC.news" />
        </Link>
        <SocialSign/>
      </div>
    );
  }
}

export default TopBar