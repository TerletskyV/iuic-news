import React from 'react';
import { NavLink } from 'react-router-dom';
import { navbarBackground, navbarLinkColor } from './assets/colors';

const styles = {
  toolbar: {
    background: navbarBackground,
    height: 45,
    padding: "0 20px"
  },
  ul: {
    alignItems: "center",
    display: "flex",
    height: "inherit",
    padding: 0,
    margin: 0,
    listStyleType: "none"
  },
  li: {
    height: "inherit"
  },
  a: {
    alignItems: "center",
    color:navbarLinkColor,
    fontWeight: 600,
    display: "flex",
    height: "inherit",
    justifyContent: "center",
    textDecoration: "none",
    padding: "0 15px"
  }
}

const ToolBar = () => (
  <div style={styles.toolbar}>
    <ul style={styles.ul}>
      <li style={styles.li}>
        <NavLink style={styles.a} exact to="/">World</NavLink>
      </li>
      <li style={styles.li}>
        <NavLink style={styles.a} to="/country">Country</NavLink>
      </li>
      <li style={styles.li}>
        <NavLink style={styles.a} to="/city">City</NavLink>
      </li>
    </ul>
  </div>
)

export default ToolBar;