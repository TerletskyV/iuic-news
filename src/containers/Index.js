import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as Actions from '../actions'

import TopBar from '../components/TopBar'
import Slider from '../components/Slider'
import NavBar from '../components/NavBar'
import Footer from '../components/Footer'
import News from '../components/News'
import AddNewsModal from '../components/modals/AddNewsModal'
import CountryButton from '../components/buttons/CountryButton'
import CitiesButton from '../components/buttons/CitiesButton'

class Index extends Component {

  componentWillMount() {
    this.props.actions.getCountriesList();
    this.props.actions.getSliderItems();
    this.props.actions.goAwayNews();
    this.props.actions.getNews();
  }

  render() {
    return (
      <div className="iuic-news">
        <TopBar />
        <div className="iuic-news__carousel">
          <div className="iuic-news__button-group">
            <CountryButton
              cities={this.props.actions.getCitiesListOfCountry}
              countries={this.props.countries}
              onGoAwayNews={this.props.actions.goAwayNews}
              countryNews={this.props.actions.getNews}>
            </CountryButton>
            <CitiesButton
              cities={this.props.cities}>
            </CitiesButton>
          </div>
          <Slider
            sliderItems={this.props.sliderItems}>
          </Slider>
        </div>
        <NavBar
          countries={this.props.countries}>
        </NavBar>
        <News
          news={this.props.news}
          page={this.props.news.page}
          onFetchNews={this.props.actions.getNews}
          onGoAwayNews={this.props.actions.goAwayNews}>
        </News>
        <Footer />
        <AddNewsModal
          countries={this.props.countries}>
        </AddNewsModal>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    news: state.news,
    newsItem: state.newsItem,
    sliderItems: state.sliderItems,
    countries: state.countries,
    cities: state.cities
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Index)