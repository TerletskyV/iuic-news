import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../actions';
import { Container, Row, Col } from 'reactstrap';

import TopBar from '../components/TopBar';
import CityInfo from '../components/city-page/info-panel/CityInfo';
import NavBar from '../components/NavBar';
import MainContent from '../components/News/MainContent';
import Rubrificator from '../components/city-page/rubrificator/Rubrificator';
import TopNews from '../components/News/TopNews';
import Footer from '../components/Footer';

const serverUrl = "https://news.iuic.info";

class Article extends Component {
  onLoadComponent() {
    let newsId = this.props.location.pathname.match(/\d+/g);
    this.props.actions.getNewsItem(newsId);
  }
  componentWillMount() {
    this.onLoadComponent();
  }
  render() {
    let isEmptyState = !isEmpty(this.props.newsItem);
    return (
      <div>
        <TopBar />
        <CityInfo cityId={this.props.newsItem.city_id} />
        <NavBar />
        <Container fluid className="custom-offset">
          <Row>
            <Col sm="4" md="3" lg="2">
              <Rubrificator
                city_id={this.props.newsItem.city_id}
                match={this.props.match}
                location={this.props.location}>
              </Rubrificator>
            </Col>
            <Col sm="5" md="7" lg="7">
              {isEmptyState ?
                <MainContent
                  title={this.props.newsItem.title_ru}
                  youtube_link={!!this.props.newsItem.youtube_link ? (this.props.newsItem.youtube_link).replace("watch?v=", "embed/") : ""}
                  audio_file={this.props.newsItem.audio_file.url}
                  image_count={this.props.newsItem.screenshots_count}
                  main_image={this.props.newsItem.title_screenshot}
                  description={this.props.newsItem.description_ru}
                  user_name={this.props.newsItem.user.first_name + ' ' + this.props.newsItem.user.last_name}
                  user_link={"/user/" + this.props.newsItem.user.id}
                  user_avatar={this.props.newsItem.user.avatar.url != null ? serverUrl + this.props.newsItem.user.avatar.url : this.props.newsItem.user.avatar_link}
                  date={this.props.newsItem.user.updated_at}
                  like={this.props.newsItem.news_like.voices}
                  dislike={this.props.newsItem.news_dislike.voices}
                  views={this.props.newsItem.views}
                  comments={!!this.props.newsItem.news_comments_count ? this.props.newsItem.news_comments.reverse() : null}
                  />
                : <h3>Loading...</h3>
              }
            </Col>
            <Col sm="3" md="2" lg="3">
              <TopNews />
            </Col>
          </Row>
        </Container>
        <Footer/>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    newsItem: state.newsItem.content
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

function isEmpty(obj) {
  return Object.keys(obj).length === 0 && obj.constructor === Object
}

export default connect(mapStateToProps, mapDispatchToProps)(Article)