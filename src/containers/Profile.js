import React, { Component } from 'react';
import { Accordion, Panel } from 'react-bootstrap';
import { Alert } from 'reactstrap';
import { Link } from 'react-router-dom';
import Icon from 'react-fontawesome';
import Auth from '../components/Profile/Auth';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../actions/index.js';

class Profile extends Component {
  constructor(props) {
      super(props);
      this.selected = this.selected.bind(this);
      this.state = {
        selected: false
      }
    }

    selected() {
      this.setState({
        selected: !this.state.selected
      })
    }

  render() {
    console.log(this)
    return (
      <div className="profile container-fluid">
        <div className="row">
          <div className="col-md-6">
            <Link to="/" className="profile__close-profile close-profile btn">
              <span className="close-profile__text">Закрыть кабинет &nbsp;</span>
              <Icon name="sign-out"></Icon>
            </Link>
            <Auth/>
          </div>
          <div className="col-md-6 profile__info">
            <div className="row profile__info-header user">
              <div className="col-lg-3">
                <div className="user__avatar img-thumbnail">
                  <img className="user__avatar-img" src={this.props.avatar ? this.props.avatar : this.props.userState.userAvatar} alt="аватар пользователя" />
                </div>
              </div>
              <div className="col-lg-9">
                <div className="user__info">
                  <div className="user__name">{this.props.name ? this.props.name : this.props.userState.userFirstName + ' ' + this.props.userState.userLastName}</div>
                  <div className="user__age">{this.props.age}</div>
                  <div className="user__address">{this.props.address}</div>
                </div>
              </div>
            </div>
            <div className="profile__info-main">
              <Accordion accordion>
                <Panel onClick={this.selected} header={
                  <div className={this.state.selected ? "profile__tab-panel--selected profile__tab-panel tab-panel" : "profile__tab-panel tab-panel"}>
                    <span>партнеры</span>
                    <span className="tab-panel__badge badge pull-right">0</span>
                  </div>
                } eventKey="1">
                  <div className="profile__panel-body">
                    <Alert color="info">
                      Здесь будут только те пользователи, которые стали Вашими партнерами.
                    </Alert>
                  </div>
                </Panel>
                <Panel header={
                  <div className="profile__tab-panel">
                    <span>диалоги</span>
                    <span className="tab-panel__badge badge pull-right">0</span>
                  </div>
                } eventKey="2">
                  <div className="profile__panel-body">
                    <Alert color="info">
                      Здесь можно вести переписку и обмен файлами со всеми, кто является пользователем IUIC.
                    </Alert>
                  </div>
                </Panel>
                <Panel header={
                  <div className="profile__tab-panel">
                    <span>приглашения</span>
                    <span className="tab-panel__badge badge pull-right">0</span>
                  </div>
                } eventKey="3">
                  <div className="profile__panel-body">
                    <Alert color="info">
                      Здесь можно получать приглашения на мероприятия от партнеров в свой календарь
                      и самому приглашать партнеров в их личном календаре.
                    </Alert>
                  </div>
                </Panel>
                <Panel header={
                  <div className="profile__tab-panel">
                    <span>публикации</span>
                    <span className="tab-panel__badge badge pull-right">0</span>
                  </div>
                } eventKey="4">
                  <div className="profile__panel-body">
                    <Alert color="info">
                      Здесь - созданные Вами публикации. Это могут быть:
                      объявления, оповещения, новости, авторские статьи и многое другое…
                    </Alert>
                  </div>
                </Panel>
                <Panel header={
                  <div className="profile__tab-panel">
                    <span>избранное</span>
                    <span className="tab-panel__badge badge pull-right">0</span>
                  </div>
                } eventKey="5">
                  <div className="profile__panel-body">
                    <Alert color="info">
                      Здесь будут только те пользователи, которые стали Вашими партнерами.
                    </Alert>
                  </div>
                </Panel>
              </Accordion>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    userState: state.userState
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
