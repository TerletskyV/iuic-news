import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import news from './news'
import sliderItems from './slider'
import countries from './countries'
import cities from './cities'
import newsItem from './newsItem'
import user from './user'
import userState from './userState'
import sections from './sections'
import sectionIdNews from './sectionIdNews'
import userAddress from './userAddress'

export default combineReducers({
  routing: routerReducer,
  news,
  newsItem,
  sliderItems,
  countries,
  cities,
  user,
  userState,
  sections,
  sectionIdNews,
  userAddress
})