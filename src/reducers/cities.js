import {
  GET_CITIES_LIST_OF_COUNTRY_PENDING,
  GET_CITIES_LIST_OF_COUNTRY_FULFILLED,
  GET_CITIES_LIST_OF_COUNTRY_REJECTED } from '../actions';

const initialState = {
  cities: [],
  pending: false,
  error: false
}

export default function newsState(state = initialState, action) {
  switch (action.type) {
    case GET_CITIES_LIST_OF_COUNTRY_PENDING:
      return {...state,
        pending: true,
        error: false
      }
    case GET_CITIES_LIST_OF_COUNTRY_REJECTED:
      return {...state,
        pending: false,
        error: true
      }
    case GET_CITIES_LIST_OF_COUNTRY_FULFILLED:
      let citiesListData = action.payload.data.data.attributes.cities,
        citiesListArr = [];
        citiesListData.forEach( (item) => {
          citiesListArr.push({
            cityNameRu: item.name_ru,
            cityId: item.id
          })
        });
      return {...state,
        cities: citiesListArr,
        pending: false,
        error: false
      }
    default:
      return state;
  }
}