import {
  GET_NEWS_ITEM_PENDING,
  GET_NEWS_ITEM_FULFILLED,
  GET_NEWS_ITEM_REJECTED } from '../actions'

const initialState = {
  content: {},
  pending: false,
  error: false
};

export default function newsState(state = initialState, action) {
  switch (action.type) {
    case GET_NEWS_ITEM_FULFILLED:
      return { ...state,
        content: action.payload.data.data.attributes,
        pending: false,
        error: false
      }
    case GET_NEWS_ITEM_PENDING:
      return { ...state,
        content: {},
        pending: true,
        error: false
      }
    case GET_NEWS_ITEM_REJECTED:
      return {...state,
        pending: false,
        error: true
      }
    default:
      return state
  }
}