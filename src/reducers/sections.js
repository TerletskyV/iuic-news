import {
  GET_SECTIONS_PENDING,
  GET_SECTIONS_FULFILLED,
  GET_SECTIONS_REJECTED } from '../actions';

const initialState = {
  sections: [],
  pending: false,
  error: false
}

export default function newsState(state = initialState, action) {
  switch (action.type) {
    case GET_SECTIONS_PENDING:
      return {...state,
        pending: true,
        error: false
      }
    case GET_SECTIONS_REJECTED:
      return {...state,
        pending: false,
        error: true
      }
    case GET_SECTIONS_FULFILLED:
      let sectionsListArr = [],
        data = action.payload.data.data;
      data.forEach( (item) => sectionsListArr.push({
        section_id: item.id,
        section_name_ru: item.attributes.name_ru
      }));
      return {...state,
        sections: sectionsListArr,
        pending: false,
        error: false
      }
    default:
      return state;
  }
}