import {
  GET_COUNTRIES_LIST_PENDING,
  GET_COUNTRIES_LIST_FULFILLED,
  GET_COUNTRIES_LIST_REJECTED } from '../actions'

const initialState = {
  countries: [],
  pending: false,
  error: false
}

export default function newsState(state = initialState, action) {
  switch (action.type) {
    case GET_COUNTRIES_LIST_PENDING:
      return {...state,
        pending: true,
        error: false
      }
    case GET_COUNTRIES_LIST_REJECTED:
      return {...state,
        pending: false,
        error: true
      }
    case GET_COUNTRIES_LIST_FULFILLED:
      let countriesListData = action.payload.data.data,
        countriesListArr = [];
      countriesListData.forEach( (item) => {
        countriesListArr.push({
          id: item.id,
          name_ru: item.attributes.name_ru
        });
      } );
      return {...state,
        countries: countriesListArr,
        pending: false,
        error: false
      }
    default:
      return state;
  }
}