import {
  GET_NEWS_PENDING,
  GET_NEWS_FULFILLED,
  GET_NEWS_REJECTED,
  GO_AWAY_NEWS } from '../actions'

const initialState = {
  list: [],
  page: 1,
  noResults: false,
  pending: false,
  error: false
};

export default function newsState(state = initialState, action) {
  switch(action.type) {
    case GET_NEWS_PENDING:
      return { ...state,
        list: [],
        pending: true
      }

    case GET_NEWS_REJECTED:
      return {...state,
        pending: false,
        error: true
      }

    case GO_AWAY_NEWS:
      return { ...state,
        list: [],
        page: 1
      }

    case GET_NEWS_FULFILLED:
      let data = action.payload.data.data;
      let newsArray = [];

      data.forEach(function(article) {
        newsArray.push(article)
      })

      return { ...state,
        list: newsArray,
        page: ++state.page,
        noResults: !data,
        pending: false,
        error: false
      }
    default:
      return state;
  }
}