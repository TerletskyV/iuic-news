import { map } from 'lodash';
import {
  GET_SLIDER_ITEMS_PENDING,
  GET_SLIDER_ITEMS_FULFILLED,
  GET_SLIDER_ITEMS_REJECTED } from '../actions';


const initialState = {
    items: [],
    pending: false,
    error: false
};

export default function newsState(state = initialState, action) {
  switch (action.type) {
    case GET_SLIDER_ITEMS_PENDING:
      return {...state,
        items: [],
        pending: true,
        error: false
      }
    case GET_SLIDER_ITEMS_REJECTED:
      return {...state,
        items: [],
        pending: false,
        error: true
      }
    case GET_SLIDER_ITEMS_FULFILLED:
      let sliderItemsData = action.payload.data,
        itemsArr = map(sliderItemsData, (value, key) => ({
        cityId: value[0],
        cityNameRu: value[1],
        cityImgUrl: value[2]
      }));
      return {...state,
        items: itemsArr,
        pending: false,
        error: false
      }
    default:
      return state
  }
}