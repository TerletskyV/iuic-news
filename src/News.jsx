import React from 'react';

const styles = {
  news: {
    alignItems: "center",
    backgroundColor: "#cdcdcd",
    border: "1px solid #000",
    display: "flex",
    height: 390,
    justifyContent: "center",
    marginTop: 5
  }
}

const News = (props) => (
  <div style={styles.news}>
    {props.children}
  </div>
)

export default News;